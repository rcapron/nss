\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}

\usepackage[english]{babel}
\usepackage{graphicx} % Required to insert images

\title{Distributed controllers}
\author{Iwan Briquemont \and Romain Capron}
\date{\today}

%----------------------------------------------------------------------------------------
%	NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{DRAFT}
\newcommand{\hmwkClassCode}{INGI2149}
\newcommand{\hmwkClassName}{Network and Security Seminar}
\newcommand{\hmwkAuthorName}{Iwan Briquemont \& Romain Capron}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\newcommand{\HRule}[1]{\rule{\linewidth}{#1}} 	% Horizontal rule

\makeatletter							% Title
\def\printtitle{%						
    {\centering \@title\par}}
\makeatother									

\makeatletter							% Author
\def\printauthor{%					
    {\centering \large \@author}}				
\makeatother							

\title{	\normalsize \textsc{\hmwkTitle} 	% Subtitle of the document
		 	\\[2.0cm]													% 2cm spacing
			\HRule{0.5pt} \\										% Upper rule
			\huge \textbf{\uppercase{\hmwkClassName \\ \hmwkClassCode}}	% Title
			\HRule{2pt} \\ [0.5cm]								% Lower rule + 0.5cm spacing
			\normalsize \today									% Todays date
		}

\author{
		\hmwkAuthorName \\	
		INGI Department\\
}

\begin{document}

\printtitle
  	\vfill
\begin{center}
	\includegraphics[width=140pt]{./image/logo}
\end{center}
\printauthor

\section{Introduction}
Software Defined Networks separate the network control logic from the network forwarding hardware, putting the logic on a software component, the controller. The controller is logically centralized and can operate on a global network view. To achieve this centralization, the simplest choice is to have a physically centralized control plane. But this solution has major drawbacks such as a loss in reactivity, reliability and scalability. The controller should be physically distributed to overcome these limitations. \cite{tootoonchian2010hyperflow, Levin:2012:LCS:2342441.2342443, Dixit:2013:TED:2491185.2491193}

\section{Expectations for distributed controllers}

% TODO: rephrase and put references, can also be more detailed for each issue
% TODO: find references (!= increase responsiveness) for : It should also give a fresh and consistent view of the network, ...
The "perfect" distributed controller should increase responsiveness: switches should get a response faster than with a single controller. \cite{tootoonchian2010hyperflow, Levin:2012:LCS:2342441.2342443} It should also give a fresh and consistent view of the network, which hides the distributed part from the application. It should be reliable: controller instances and parts of the network may fail, possibly with partitioning, and the controller should still be available and consistent. \cite{tootoonchian2010hyperflow, Levin:2012:LCS:2342441.2342443, Dixon:2011:ESF:1972457.1972467} It should be scalable: the controller should scale to larger networks, or networks with heavier load, by adding more controller instances. \cite{tootoonchian2010hyperflow, Levin:2012:LCS:2342441.2342443, Dixit:2013:TED:2491185.2491193, Schmid:2013:ELD:2491185.2491198} It should be elastic: it should react to changes in the network load and it should be possible to add network equipment and controller instances without disrupting the service. \cite{Dixit:2013:TED:2491185.2491193, Schmid:2013:ELD:2491185.2491198, Yu:2010:SFN:2043164.1851224} It should support applications made for single controllers with minimum changes. \cite{tootoonchian2010hyperflow, Dixit:2013:TED:2491185.2491193, Yu:2010:SFN:2043164.1851224} It should be compatible with OpenFlow that is currently supported by a lot of network equipment provider. \cite{tootoonchian2010hyperflow, Dixit:2013:TED:2491185.2491193, Yu:2010:SFN:2043164.1851224}

Of course, the more properties we want and the more complex the controller will be. There are some trade-offs to be made between complexity and the number of features, as well as between some features, e.g.: having a consistent view will delay updates, thus reducing responsiveness.

\section{Existing solutions}

A simple way to achieve some distribution of the controller is to split the network in \emph{controller domains} and put a (single or distributed) controller in each part, responsible for its domain. We of course loses the global network view, but depending on the needs for the complete network, local algorithms may be used \cite{Schmid:2013:ELD:2491185.2491198} to coordinate the controllers. With local algorithms, each controller domain only need a constant number of communication steps to solve a task, and it is usually less expensive than managing a consistent global network view. However it has issues, first the algorithm is not general: for each task a specific \emph{distributed} algorithm must be designed. Second, a local algorithm does not exists for all problems SDN solves. This distribution scheme is quite limited, but it can still make sense where the computation needed on the full network view is specific and can be solved with a local algorithm, as this gives a lightweight solution while limiting the growth of the controller domains.

HyperFlow \cite{tootoonchian2010hyperflow} and Onix \cite{koponen2010onix} are distributed controllers which allow for multiple controllers to be used in a single domain. They both scale to larger (or more heavily used) networks by having more controllers. 

With HyperFlow, each controller holds the full state which is consistently updated with the other controllers. This improves the responsiveness as a controller can directly answer any request using its local state, the drawback being that the state might become stale, especially if the synchronization load grows. HyperFlow is resilient to network partitioning and controller failures. It also distribute state updates as to limit cross-site (a site is a highly-connected component of the network with high bandwidth) control traffic, controllers mostly get updated by their controllers. The evaluation of HyperFlow is limited and only assesses performance.
% TODO: try to put numbers by using evaluation of the paper.

Onix divides the state in two parts: a strongly consistent part for slow updates, using a database, and an eventually consistent part for fast network changes, using a Distributed Hash Table.

Onix makes a trade-off between strong consistency and speed, a general analysis of this trade-off is detailed in \cite{Levin:2012:LCS:2342441.2342443}, which researches the impact of strong consistency or eventual consistency with regards to speed and correctness of the algorithm.

DIFANE \cite{Yu:2010:SFN:2043164.1851224} takes another approach: it aims to optimise the flow control speed by preinstalling rules on the switches themselves instead of having them ask controllers. It works by having \emph{authority switches} which play a special role in the network. The (single) controller distributes the rules in advance between these switches, to be able to handle large rule sets. As rules are usually computed into microflow rules (e.g.: one rule per IP), the number of rules grows quickly. To limit this problem, as cache memory on switches is limited, DIFANE supports wildcard rules which sum up many microflow rules. The authority switches know how to forward flows, so the other ingress switches encapsulate and forward their unknown packets to the authority switches using \emph{partition rules}. A caching mechanism is used to avoid overloading the authority switches: whenever a packet matches the authority rules in the authority switch, a control plane caching function is triggered which installs cache rules in the ingress switch, and this can be done concurrently. Once the cache rules are installed, the ingress switches encapsulates and forwards the matching packets directly to egress switches.

The main advantage of this solution is performance: switches do not have to ask a controller how to forward unknown flows, everything stays in the data plane, thus avoiding the first packet delay. The maximum number of flow setups per second is also increased as the controller is no longer a bottleneck. When rules need to be changed the solution is a bit complex but it is supported.

% TODO: add ETTM (in progress)
ETTM \cite{Dixon:2011:ESF:1972457.1972467} manages securely and efficiently network resources at a packet granularity.

All previous solutions assume a fixed mapping between the switches and the controllers, which means adding, removing and moving controllers must be done by hand by reconfiguring the switches. This task can be tedious and is prone to issues in guaranteeing consistency and reliability. This highlights the need for an \emph{elastic distributed controller architecture} and ElastiCon \cite{Dixit:2013:TED:2491185.2491193} addresses these problems.

\section{Conclusion}

\nocite{*}
\bibliographystyle{plain}
\bibliography{distributed-controllers}

\end{document}
